module.exports = {
    navigateFallback: '/index.html',
    stripPrefix: 'dist/',
    replacePrefix: 'AngTodo/',
    root: 'dist/',
    staticFileGlobs: [
      'dist/index.html',
      'dist/manifest.json',
      'dist/**.js',
      'dist/**.css',
      'dist/assets/**.js',
      'dist/assets/**.css',
      'dist/assets/bootstrap/css/**.css',
      'dist/assets/bootstrap/js/**.js',
      'dist/assets/font-awesome/css/**.css',
      'dist/assets/font-awesome/fonts/*.eot',
      'dist/assets/font-awesome/fonts/*.svg',
      'dist/assets/font-awesome/fonts/*.ttf*',
      'dist/assets/font-awesome/fonts/*.woff*',
      'dist/assets/font-awesome/fonts/*.woff2*',
      'dist/assets/font-awesome/fonts/*.otf',
      'dist/assets/font-awesome/fonts/*.*',
      'dist/assets/font-awesome/fonts/*.eot',
      'dist/assets/font-awesome/less/*.less',
      'dist/assets/font-awesome/scss/*.scss',
      'dist/assets/img/*.png',
      'dist/assets/jquery/*.js',
      'dist/assets/popper/*.js'
    ]
  };