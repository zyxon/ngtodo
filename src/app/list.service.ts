import { Injectable } from '@angular/core';

@Injectable()
export class ListService {

  getTodos(): Promise<string[]> {
    try {
      let storedContent = JSON.parse(localStorage.AngTodos);
      return Promise.resolve(storedContent);
    } catch (e) {
      return Promise.resolve([]);;
    }
  }

  putTodo(todo: string): Promise<void> {
    if (todo !== "" && todo !== null) {
      this.getTodos().then(storedContent => {
        storedContent.push(todo);
        localStorage.AngTodos = JSON.stringify(storedContent);
      });
    }

    return Promise.resolve();
  }

  deleteTodo(index: number): Promise<void> {
    if (!isNaN(index) && index != null) {
      this.getTodos().then(storedContent => {
        storedContent.splice(index, 1);
        localStorage.AngTodos = JSON.stringify(storedContent);
      });
    }

    return Promise.resolve();
  }

  updateTodo(index: number, text: string): Promise<void> {
    this.getTodos().then((storedContent) => {
      if (!isNaN(index) && index != null && index >= 0 && index < storedContent.length &&
        text != null && text !== "") {
        storedContent[index] = text;
        localStorage.AngTodos = JSON.stringify(storedContent);
      }

    });
    return Promise.resolve();
  }

}