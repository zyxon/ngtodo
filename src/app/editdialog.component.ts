import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ListComponent } from './list.component';
import { Injectable } from '@angular/core';

declare var jQuery: any;

@Component({
  selector: 'edit-todo-dialog',
  templateUrl: './editdialog.component.html',
  styleUrls: ['./editdialog.component.css'],
})
export class EditTodoDialogComponent {

  @Output()
  @Input() currentText: string = "";
  @Input() index: number;

  @Output() onTodoUpdated = new EventEmitter();

  inputFocused: boolean = false;

  constructor() {

  }

  submitEdit(): void {
    let updatedTodo = {
      index: this.index,
      text: this.currentText
    };

    this.onTodoUpdated.emit(updatedTodo);
  }

  show(): void {
    jQuery("#edit_todo_modal").modal(); // show bootstrap modal
    this.inputFocused = true;
  }

  handleEditEnter(): void {
    jQuery("#edit_todo_modal").modal("hide");
    this.submitEdit();
  }
}