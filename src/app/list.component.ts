import { Component, trigger, transition, style, animate, state } from '@angular/core';

import { ListService } from './list.service';
import { OnInit } from '@angular/core';
import { EditTodoDialogComponent } from './editdialog.component';

/**
 * The main todo list component
 */

@Component({
  selector: 'list-component',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ListService, EditTodoDialogComponent],
  animations: [
    trigger(
      'fadeIn',
      [
        transition(
          ':enter',[
            style({opacity: 0}),
            animate('150ms', style({opacity: 1}))
          ]
        )
      ]
    ),
    trigger(
      'fadeInSlow',
      [
        transition(
          ':enter',[
            style({opacity: 0}),
            animate('600ms', style({opacity: 1}))
          ]
        )
      ]
    ),
    trigger(
      'spin',
      [
        transition(
          ':enter',[
            animate('300ms', style({transform: 'rotate(-360deg)'})),
            animate('200ms', style({transform: 'scale(1.5)'})),
            animate('200ms', style({transform: 'scale(-1.5)'})),
            animate('300ms', style({transform: 'rotate(360deg)'}))
          ]
        )
      ]
    )
  ]
    
})
export class ListComponent implements OnInit {
  
  imagePath: string = "./AngTodo/assets/img/angular.png";
  title: string = 'ngTodo';

  private listService: ListService;
  private editTodoDialog: EditTodoDialogComponent;
  todos: Array<string> = [];
  newTodoVal: string = "";
  showEmptyWarning: boolean = false;

  editIndex: number;
  editCurrentText: string;


  constructor(listService: ListService, editTodoDialog: EditTodoDialogComponent) {
    this.listService = listService;
    this.editTodoDialog = editTodoDialog;
  }

  ngOnInit(): void {
    this.getTodos();
  }

  getTodos(): void {
    this.listService.getTodos().then(todos => {
      this.todos = todos;
    });
  }

  addTodo(todo: string): void {
    if (todo !== "" && todo !== null) {
      this.listService.putTodo(todo).then(() => this.getTodos());
      this.showEmptyWarning = false;
      this.newTodoVal = "";
    } else {
      this.showEmptyWarning = true;
    }
  }

  removeTodo(index: number): void {
    this.listService.deleteTodo(index).then(() => this.getTodos());
  }

  // TO BE IMPLEMENTED

  showEditTodoDialog(index: number): void {
    this.editIndex = index;
    this.editCurrentText = this.todos[index];
    this.editTodoDialog.show();
  }

  editTodo(updatedTodo: object): void {

    let uindex = updatedTodo["index"];
    let utext = updatedTodo["text"];

    if (!isNaN(uindex) && uindex !== null && uindex >= 0
      && utext !== null && utext !== "") {
      console.log("most jön az apdét: " + uindex + " :: " + utext);

      this.listService.updateTodo(uindex, utext).then(() => {
        this.getTodos();
      });
    }
  }
}